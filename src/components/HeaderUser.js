import React from 'react';
import { NavLink } from 'react-router-dom';

const HeaderUser = () => {
    return (
        <div className="Header-User">
            <div className="header-user-left">
                <NavLink to="/">Course</NavLink>
                <span className="dot"> .</span>
            </div>
            <div className="header-user-right">
                <img src="https://img.icons8.com/metro/2x/4a90e2/user-male.png" alt="user" className="toggle-active" />
                <span className="toggle-active">Username</span>
                <span className="icon-nav toggle-active">&#8249;</span>
            </div>
            <div className="header-user-toggle" id="headerUserToggle">
                <ul>
                    <li><NavLink to="/dashboard">Dashboard</NavLink></li>
                    <li><NavLink to="/pelajaran">Pelajaran</NavLink></li>
                    <li><NavLink to="/pengaturan-akun">Pengaturan Akun</NavLink></li>
                    <li><NavLink to="/bantuan">Bantuan</NavLink></li>
                    <li><NavLink to="/">Logout</NavLink></li>
                </ul>
            </div>
        </div>
    )
}

export default HeaderUser;