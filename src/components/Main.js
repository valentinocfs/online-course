import React from 'react';
import { Link } from 'react-router-dom';

class Main extends React.Component {
    render() {
        return (
            <div className="main">
                <div className="main-background">
                    <h1 className="title">Belajar Coding Bersama Kami.</h1>
                    <h1 className="title2">Bangun Karirmu sebagai Developer Professional.</h1>
                    <div className="btn">
                       <Link to="/pelajaran" className="button active">Mulai</Link>
                    </div>
                </div>
                <div className="another-title">
                    <h1>Alasan kenapa harus memilih Course <span className="dot">.</span></h1>
                </div>
                <div className="cards">
                    <div className="card">
                        <div className="card-title">
                            <h3>Kurikulum standar industri global</h3>
                            <div className="card-content">
                                <p>Kurikulum dikembangkan bersama perusahaan dan pemilik teknologi dunia sesuai kebutuhan industri terkini.</p>
                            </div>
                        </div>
                    </div>
                    <div className="card">
                        <div className="card-title">
                            <h3>Pembelajaran Secara Terpandu</h3>
                            <div className="card-content">
                                <p>Ikuti serangkaian pelajaran yang disusun secara optimal untuk membantu Anda menjadi seorang kreator sejati.
                            Setelah selesai, Anda dapat menjadi coder dengan pengetahuan yang luas untuk mencapai cita-cita Anda secara mandiri.</p>
                            </div>
                        </div>
                    </div>
                    <div className="card">
                        <div className="card-title">
                            <h3>Belajar fleksibel sesuai jadwal Anda</h3>
                            <div className="card-content">
                                <p>Belajar kapan pun, di mana pun, secara mandiri. Bebas memilih kelas sesuai minat belajar. Akses seumur hidup ke kelas dan forum diskusi setelah lulus.</p>
                            </div>
                        </div>
                    </div>
                    <div className="card">
                        <div className="card-title">
                            <h3>Alumni terpercaya di berbagai perusahaan</h3>
                            <div className="card-content">
                                <p>Sertifikat yang membuktikan pengetahuan fundamental beserta keterampilan nyata yang diinginkan perusahaan global.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Main;