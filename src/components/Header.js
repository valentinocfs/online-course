import React from 'react';
import { Link } from 'react-router-dom';

class Header extends React.Component{
    render(){
        return(
            <div className = "header">
                <div className="header-left">
                    <Link to="/" className ="logo">Course <span className="dot">.</span></Link>
                </div>    
                <div className = "header-right">
                    <Link to="/pelajaran" className="secondary">Pelajaran</Link>
                    <span className="rectangle">|</span>
                    <Link to="/login" className="active">Login</Link>
                </div>    
            </div>    
        );
    }
}

export default Header;
