import React from 'react';
import { Link } from 'react-router-dom'

class Footer extends React.Component {
    render() {
        return (
            <div className="footer">
                <div className="footer-logo">
                    <p>Course</p>
                </div>
                <div className="footer-right">
                    <div className="layanan">
                        <h3>Layanan</h3>
                        <ul className="box">
                            <li><Link to="/pelajaran">Pelajaran</Link></li>
                        </ul>
                    </div>
                    <div className="tentang">
                        <h3>Tentang</h3>
                        <ul className="box">
                            <li><Link to="/bantuan">Bantuan</Link></li>
                        </ul>
                    </div>
                    <div className="sosial-media">
                        <h3>Kontak</h3>
                        <ul className="box">
                            <li><a href="https://instagram.com/">Instagram</a></li>
                            <li className="facebook"><a href="https://facebook.com/">Facebook</a></li>
                            <li className="youtube"><a href="https://youtube.com/">Youtube</a></li>
                        </ul>
                    </div>
                </div>

                <div className ="footer-bottom">
                    <p> &copy;Copyright 2020 All Right Reserved</p>
                </div>
            </div>
        );
    }
}

export default Footer;