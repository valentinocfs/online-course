import React, { Fragment } from 'react';
// Files
import HeaderUser from "../components/HeaderUser"
import Footer from "../components/Footer";
import "./Dashboard.css"
import { Link } from 'react-router-dom';

const Dashboard = () => {
    return(
        <Fragment>
            <HeaderUser />
            <div className='Dashboard'>
                <div className="Dashboard-userInfo">
                    <div className="userInfo-profile">
                        <div>
                            <img src="https://img.icons8.com/metro/2x/4a90e2/user-male.png" alt="user" />
                        </div>
                        <div>
                            <p className="Dashboard-title-box">Username Here</p>
                            <span>Level 1</span>
                        </div>
                    </div>
                    <div className="userInfo-lesson">
                        <p className="Dashboard-title-box">Pelajaran Diselesaikan</p>
                        <div>
                            <p>0 Pelajaran</p>
                            <p>0 Tantangan</p>
                        </div>
                    </div>
                    <div className="userInfo-submission">
                        <p className="Dashboard-title-box">Submission Diselesaikan</p>
                        <div>
                            <p>0 Submission</p>
                            <p>0 Submission</p>
                        </div>
                    </div>
                </div>
                <div className="Dashboard-lesson-progress">
                    <h3>Pelajaran Berlangsung</h3>
                    <div className="Dashboard-box-lesson-progress">
                        <p>Tidak ada pelajaran berlangsung</p>
                    </div>
                </div>
                <div className="Dashboard-lesson-recom">
                    <h3>Rekomendasi Pelajaran</h3>
                    <div className="Dashboard-box-lesson-recom">
                        <div className="lesson-recom-cards">
                            <div className="recom-card recom-html">
                                <img src="https://img.icons8.com/color/2x/html-5.png" alt="html" />
                                <h4>HTML & CSS</h4>
                                <p>Bahasa Markup untuk membuat dan mendesain sebuah tampilan website</p>
                                <Link to="/pelajaran/pelajaran-detail/1">Mulai</Link>
                            </div>
                            <div className="recom-card recom-javascript">
                                <img src="https://img.icons8.com/color/2x/javascript.png" alt="javasript" />
                                <h4>Javascript</h4>
                                <p>Bahasa Pemrograman yang bisa digunakan untuk membuat web, server, hingga mobile app</p>
                                <Link to="/pelajaran/pelajaran-detail/2">Mulai</Link>
                            </div>
                            <div className="recom-card recom-react">
                                <img src="https://img.icons8.com/color/2x/react-native.png" alt="react" />
                                <h4>React JS</h4>
                                <p>Library Javascript terpopuler untuk membuat UI</p>
                                <Link to="/pelajaran/pelajaran-detail/7">Mulai</Link>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <Footer />
        </Fragment>
    )
}

export default Dashboard;