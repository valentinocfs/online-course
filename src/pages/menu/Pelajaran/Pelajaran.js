import React, { Fragment } from "react";
import HeaderUser from "../../../components/HeaderUser"
import Footer from "../../../components/Footer";
import dataPelajaran from "./dataPelajaran.json";
import "./css/Pelajaran.css";
import { NavLink } from "react-router-dom";

const Pelajaran = () => { 
    const lessons = dataPelajaran;

    return(
        <div className="Pelajaran">
           <HeaderUser /> 
           <main className="main-lessons">
               <h3>Daftar Pelajaran</h3>
               <section className="daftar-pelajaran">
                    <div className="cards-lessons">
                       <CardLesson data={lessons} />                       
                    </div>
               </section>
           </main>
           <Footer />
        </div>
    )
}

const CardLesson = (props) => {
    const { data } = props;

    return (
        <Fragment>
            {   
                data.map(item => {
                    return(
                        <div className="card-lesson" key={item.id}>
                            <div className="logo-lesson">
                                <img src={`${item.logo}`} alt={item.judul} />
                            </div>
                            <h4>{item.judul}</h4>
                            <p>{item.deskripsi}</p>
                            <div className="btn-wrapper">
                                <NavLink to={`/pelajaran/pelajaran-detail/${item.id}`} className="btn-lesson">
                                    Mulai Belajar
                                </NavLink>   
                            </div>
                        </div>
                    )
                })
            } 
        </Fragment>
    )
}

export default Pelajaran;