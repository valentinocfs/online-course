import React, { Fragment } from 'react';
import { useParams } from 'react-router';
// Files
import HeaderUser from "../../../components/HeaderUser"
import Footer from '../../../components/Footer';
import dataPelajaran from "./dataPelajaran.json";
import "./css/PelajaranDetail.css"
import { NavLink } from 'react-router-dom';

const PelajaranDetail = () => {
    const { id } = useParams();
    const lessons = dataPelajaran;
    const lesson = lessons[id - 1]

    return (
        <Fragment>
            <HeaderUser />
            <div className="PelajaranDetail">
                <div className="lesson-detail-title">
                    <h3>{lesson.judul}</h3>
                    <NavLink to="/pelajaran" className="btn-back-lesson">Kembali</NavLink>
                </div>
                <div className="lesson-detail-desc">
                    <img src={`${lesson.logo}`} alt={lesson.judul} />
                    <p>{lesson.judul + " adalah " +lesson.deskripsi}</p>
                </div>
                <div className="lesson-detail-study">
                    <h4>Silabus</h4>
                    <div className="detail-study-boxs">
                        {
                            lesson.materi.map((item, idx) => {
                                return (
                                    <div key={idx} className="study-box">
                                        <p >{item}</p>
                                        <NavLink to={`/pelajaran/pelajaran-detail/${lesson.id + 1}/${idx + 1}`} className="btn-start-study">Mulai</NavLink>
                                    </div>
                                )
                            })
                        }
                    </div>
                    <h6>Jumlah Silabus : {lesson.silabus}</h6>
                </div>
            </div>
            <Footer />
        </Fragment> 
    )  
} 

export default PelajaranDetail;