import React, { Fragment } from "react";
import HeaderUser from "../../../components/HeaderUser.js"
import Footer from "../../../components/Footer.js";
import "./PengaturanAkun.css"

const PengaturanAkun = () => { 
    return(
      <Fragment>
          <HeaderUser/>
        <div className="PengaturanAkun">
           <div className="pengaturan-title">
              <h3>Pengaturan Akun</h3>
           </div>
            <main className="pengaturan-box">
                <div className="box-title">
                    Profile Pengguna
                </div>
                <div className="on-box">
                    <div className="profile-image">
                        <div className="profile"></div>
                        <input type="submit" value="Ubah Foto" className="profile-btn"/>
                    </div>
                    <div className="form-on-box">
                        <p>Nama Lengkap <span className="star">*</span></p>
                        <input type="text" className="input-box" />

                        <p>Username <span className="star">*</span></p>
                        <input type="text" className="input-box" />

                        <p>Email <span className="star">*</span></p>
                        <input type="text" className="input-box"/>

                        <p>Bio</p>
                        <input type="text" className="input-box-bio" />

                    </div>
                    <input type="submit" value="Simpan Perubahan" className="profile-submit-btn"/>
                </div>
            </main>
        </div>
        <Footer/>
        </Fragment>  
    );
}

export default PengaturanAkun;