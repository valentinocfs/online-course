import React, { Fragment } from "react";
import { NavLink } from "react-router-dom";
import HeaderUser from "../../../components/HeaderUser.js"
import Footer from "../../../components/Footer.js";
import "./Bantuan.css"

const Bantuan = () => {
    return (
        <Fragment>
            <HeaderUser />
            <div className="Bantuan">
                <div className="bantuan-title">
                    <h3>Kategori Bantuan</h3>
                </div>
                <div className="bantuan-box">
                    <div className="bantuan">
                        <div className="getting-started">
                            <h4>Getting Started</h4>
                            <ul className="box-bantuan">
                                <li><NavLink to="/login">Login</NavLink></li>
                                <li><NavLink to="/daftar">Register</NavLink></li>
                                <li><NavLink to="/pengaturan-akun">Verified Email</NavLink></li>
                            </ul>
                        </div>
                        <div className="pelajaran">
                            <h4>Pelajaran</h4>
                            <ul className="box-bantuan">
                                <li><NavLink to="/pelajaran">Pelajaran Umum</NavLink></li>
                                <li><NavLink to="/pelajaran">Sertifikat</NavLink></li>
                                <li><NavLink to="/pengaturan-akun">Berlangganan</NavLink></li>
                            </ul>
                        </div>
                        <div className="pengaturan-akun">
                            <h4>Pengaturan Akun</h4>
                            <ul className="box-bantuan">
                                <li><NavLink to="/pengaturan-akun">Mengubah Informasi</NavLink></li>
                                <li><NavLink to="/pengaturan-akun">Penghapusan Akun</NavLink></li>
                            </ul>
                        </div>
                        <div className="masalah-lainnya">
                            <h4>Masalah Lainnya</h4>
                            <ul className="box-bantuan">
                                <li><a href="https://gmail.com">Hubungi Kami</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <Footer />
        </Fragment>
    )
}

export default Bantuan;