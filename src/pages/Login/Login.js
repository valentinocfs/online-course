import React, { Fragment } from "react";
import { NavLink } from "react-router-dom";
import "./css/Login.css"

const Login = () => { 
    return(
        <Fragment>    
            <div className="login-body">
                <div className="center">
                    <h1> Login </h1>
                    <form>
                        <div className="txt-field">
                            <input type="text" required />
                            <span className="underline"></span>
                            <label>Username</label>
                        </div>
                        <div className="txt-field">
                            <input type="password" required />
                            <span className="underline"></span>
                            <label>Password</label>
                        </div>
                        <div className="pass">Lupa Password?</div>
                            <NavLink to="/dashboard">
                                <input className="submit-btn" type="submit" value="Login" />
                            </NavLink>
                        <div className="signup-link">
                            Belum punya akun ? <NavLink to="/daftar">Buat Disini</NavLink>
                        </div>
                    </form>
                </div>
            </div>
            {
                /* <Footer /> */
                /*  Tidak Cocok */
            }
        </Fragment>
    )
}

export default Login;