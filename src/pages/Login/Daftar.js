import React from "react";
import { NavLink } from "react-router-dom";
import "./css/Login.css"

const Daftar = () => { 
    return(
        <div className="login-body">
            <div className="center">
                <h1> Daftar </h1>
                <form>
                    <div className="txt-field">
                        <input type="text" required />
                        <span className="underline"></span>
                        <label>Email</label>
                    </div>    
                    <div className="txt-field">
                        <input type="text" required />
                        <span className="underline"></span>
                        <label>Username</label>
                    </div>
                    <div className="txt-field">
                        <input type="password" required />
                        <span className="underline"></span>
                        <label>Password</label>
                    </div>
                    <NavLink to="/login">
                        <input className="submit-btn" type="submit" value="Daftar" />
                    </NavLink>
                    <div className="signup-link">
                    </div>
                </form>
            </div>
      </div>

    )
}

export default Daftar;