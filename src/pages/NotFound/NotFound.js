import React from "react";

const NotFound = () => { 
    return(
        <div className="NotFound" style={
            {height: "100vh" ,display: "flex", justifyContent: "center", alignItems: "center", fontSize: "3rem"}
        }>
           404 Page Not Found
        </div>
    )
}

export default NotFound;