// Library
import {Switch} from "react-router-dom"
import {Route} from 'react-router';
// Pages
import Dashboard from '../pages/Dashboard';
import Home from "../pages/Home/Home";
import Login from "../pages/Login/Login"
import Daftar from "../pages/Login/Daftar";
import Bantuan from "../pages/menu/Bantuan/Bantuan";
import Pelajaran from "../pages/menu/Pelajaran/Pelajaran";
import PelajaranDetail from "../pages/menu/Pelajaran/PelajaranDetail";
import PengaturanAkun from "../pages/menu/PengaturanAkun/PengaturanAkun";
import NotFound from "../pages/NotFound/NotFound";

const Router = () => {
    return(
        <Switch>
            <Route exact path="/" component={Home} />
            <Route path="/dashboard" component={Dashboard} />
            <Route path="/login" component={Login} />
            <Route path="/daftar" component={Daftar} />
            <Route exact path="/pelajaran" component={Pelajaran} />
            <Route exact path="/pelajaran/pelajaran-detail/:id" component={PelajaranDetail} />
            <Route path="/pengaturan-akun" component={PengaturanAkun} />
            <Route path="/bantuan" component={Bantuan} />
            <Route path="*" component={NotFound} />     
        </Switch>
    )
}

export default Router;