import React from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter as Router} from 'react-router-dom'
import './index.css';
import App from './App';

window.addEventListener('click', function(e){
  const toggle = document.getElementById("headerUserToggle")
  if(e.target.className === "toggle-active"){
     toggle.classList.toggle("nav-active")
  }
})


ReactDOM.render(
  <React.StrictMode>
    <Router>
      <App />
    </Router>
  </React.StrictMode>,
  document.getElementById('root')
);
